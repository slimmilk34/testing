import os

# The unit test artifact should be in the coverage directory per the yml file
for entry in os.scandir('./coverage'):
    if entry.is_file():
        print(entry.name)

